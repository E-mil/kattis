#include "stdio.h"
#include "stdlib.h"

int main() {
	char a[5], b[5];
	scanf("%s %s", a, b);
	double x, y;
	x = atoi(a);
	y = atoi(b);
	printf("%d\n", (int)(x * (y - 1) + 1));
	return 0;
}