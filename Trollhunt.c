#include "stdio.h"

int main() {
	int b, k, g;
	scanf("%d %d %d", &b, &k, &g);
	int d = (b - 1 + (k / g - 1)) / (k / g);
	printf("%d\n", d);
	return 0;
}