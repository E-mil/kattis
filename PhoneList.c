#include "stdio.h"

char* printConsistent(int n);

int main() {
	int t, n;
	scanf("%d\n", &t);
	char* temp[t];
	for (int i = 0; i < t; i++) {
		scanf("%d", &n);
		temp[i] = printConsistent(n);
	}
	for (int i = 0; i < t; i++) {
		printf("%s\n", temp[i]);
	}
}

char* printConsistent(int n) {
	char numbers[n][20];
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < 20; j++) {
			numbers[i][j] = 0;
		}
	}
	for (int i = 0; i < n; i++) {
		scanf("%s", &numbers[i]);
	}
	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n; j++) {
			for (int k = 0; k < 20; k++) {
				if (numbers[i][k] != numbers[j][k]) {
					if (numbers[i][k] == 0 || numbers[j][k] == 0)
						return "NO";
					break;
				}
			}
		}
	}
	return "YES";
}