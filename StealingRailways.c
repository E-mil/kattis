#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "limits.h"

int bfs(int nodes, int graph[nodes][nodes], int parent[nodes], int visited[nodes]);
int fordFulkerson(int nodes, int visited[nodes], int originalGraph[nodes][nodes]);
void removeConnection(int nodes, int n, int originalGraph[nodes][nodes], int connections[nodes][3]);

int main() {
	char sNodes[10], sEdges[10], sThreshold[10], sRailways[10];
	int nodes, edges, threshold, railways;
	scanf("%s %s %s %s", sNodes, sEdges, sThreshold, sRailways);
	nodes = atoi(sNodes);
	edges = atoi(sEdges);
	threshold = atoi(sThreshold);
	railways = atoi(sRailways);
	int originalGraph[nodes][nodes];
	memset(originalGraph, 0, nodes*nodes*sizeof(int));
	int connections[edges][3];
	memset(connections, 0, edges*3*sizeof(int));
	for (int i = 0; i < edges; i++) {
		char sN1[10], sN2[10], sFlow[10];
		int n1, n2, flow;
		scanf("%s %s %s", sN1, sN2, sFlow);
		n1 = atoi(sN1);
		n2 = atoi(sN2);
		flow = atoi(sFlow);
		originalGraph[n1][n2] = flow;
		originalGraph[n2][n1] = flow;
		connections[i][0] = n1;
		connections[i][1] = n2;
		connections[i][2] = flow;
	}
	int removeOrder[railways];
	for (int i = 0; i < railways; ++i) {
		scanf("%d", &removeOrder[i]);
	}

	/*Ford-Fulkerson*/
	int visited[nodes];
	int maxFlow = fordFulkerson(nodes, visited, originalGraph);
	int lastMaxFlow = 0;
	int k = 0;

	int nisse = 0;

	while (maxFlow >= threshold) {
		lastMaxFlow = maxFlow;
		if (k == railways) {
			k++;
			nisse = 1;
			break;
		}
		while (maxFlow - connections[removeOrder[k]][2] - connections[removeOrder[k + 1]][2] - connections[removeOrder[k + 2]][2] >= threshold && railways - k > 2) {
			maxFlow -= connections[removeOrder[k]][2];
			removeConnection(nodes, removeOrder[k], originalGraph, connections);
			k++;
		}
		removeConnection(nodes, removeOrder[k], originalGraph, connections);
		k++;
		maxFlow = fordFulkerson(nodes, visited, originalGraph);
	}

	if (nisse == 0) {
		connections[removeOrder[k-1]][2] = 1;
	}

	int minCut[edges];
	memset(minCut, 0, edges*sizeof(int));
	int l = 0;
	for (int i = 0; i < edges; i++) {
		if (connections[i][2] != 0) {
			int n1v = visited[connections[i][0]];
			int n2v = visited[connections[i][1]];
			if (n1v + n2v == 1) {
				minCut[l] = i;
				l++;
			}
		}
	}

	printf("%d %d %d\n", k - 1, lastMaxFlow, l);
	for (int i = 0; i < l; ++i) {
		printf("%d ", minCut[i]);
	}
	printf("\n");
	return 0;
}

void removeConnection(int nodes, int n, int originalGraph[nodes][nodes], int connections[nodes][3]) {
	connections[n][2] = 0;
	originalGraph[connections[n][0]][connections[n][1]] = 0;
	originalGraph[connections[n][1]][connections[n][0]] = 0;
}

int fordFulkerson(int nodes, int visited[nodes], int originalGraph[nodes][nodes]) {
	int maxFlow = 0;
	int tempGraph[nodes][nodes];
	for (int i = 0; i < nodes; i++) {
		for (int j = 0; j < nodes; ++j) {
			tempGraph[i][j] = originalGraph[i][j];
		}
	}
	int parent[nodes];
	memset(parent, 0, nodes*sizeof(int));
	while (bfs(nodes, tempGraph, parent, visited) == 1) {
		int i = nodes - 1;
		int min = INT_MAX;
		while (parent[i] != -1) {
			if (tempGraph[i][parent[i]] < min)
				min = tempGraph[i][parent[i]];
			i = parent[i];
		}
		i = nodes - 1;
		maxFlow += min;
		while (parent[i] != -1) {
			tempGraph[i][parent[i]] -= min;
			tempGraph[parent[i]][i] -= min;
			i = parent[i];
		}
	}
	return maxFlow;
}

int bfs(int nodes, int graph[nodes][nodes], int parent[nodes], int visited[nodes]) {
	memset(visited, 0, nodes*sizeof(int));
	visited[0] = 1;
	int queue[nodes*nodes];
	memset(queue, 0, nodes*nodes*sizeof(int));
	parent[0] = -1;

	int i = 0;
	int k = 1;
	while (k != 0) {
		int j = queue[i];
		for (int a = 0; a < nodes; ++a) {
			/*printf("graph[%d][%d] = %d\n", j, a, graph[j][a]);*/
			if (graph[j][a] > 0 && visited[a] == 0) {
				parent[a] = j;
				visited[a] = 1;
				queue[i + k] = a;
				k++;
			}
		}
		i++;
		k--;
	}
	return visited[nodes - 1];
}