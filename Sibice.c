#include "stdio.h"
#include "math.h"

int main() {
	int n, w, h;
	scanf("%d %d %d", &n, &w, &h);
	int m = sqrt(w * w + h * h);
	for (int i = 0; i < n; i++) {
		scanf("%d", &w);
		if (w > m)
			printf("NE\n");
		else
			printf("DA\n");
	}
	return 0;
}