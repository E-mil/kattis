#include "stdio.h"
#include "math.h"
#include "stdlib.h"

int main() {
	char a[20], b[5];
	scanf("%s %s", a, b);
	int x, y;
	x = atoi(a);
	y = atoi(b);
	int max = 1;
	for (int i = 1; i <= y; ++i) {
		max += (y - (i - 1)) * pow(2, i);
	}
	if (x <= max)
		printf("yes\n");
	else
		printf("no\n");
	return 0;
}