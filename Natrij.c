#include "stdio.h"

int main() {
	int h, m, s, a, b, c;
	scanf("%d:%d:%d", &h, &m, &s);
	scanf("%d:%d:%d", &a, &b, &c);
	if (c < s) {
		c += 60;
		b -= 1;
	}
	if (b < m) {
		b += 60;
		a -= 1;
	}
	if (a < h) {
		a += 24;
	}
	h = a - h;
	m = b - m;
	s = c - s;
	if (h == 0 && m == 0 && s == 0)
		h = 24;
	if (h < 10)
		printf("0");
	printf("%d:", h);
	if (m < 10)
		printf("0");
	printf("%d:", m);
	if (s < 10)
		printf("0");
	printf("%d", s);
	return 0;
}