#include "stdio.h"
#include "string.h"

int main() {
	char input[100];
	memset(input, 0, sizeof(input)*sizeof(char));
	scanf("%s", &input);
	printf("%c", input[0]);
	for(int i = 0; i < 100; i++) {
		if (input[i] == '-') {
			printf("%c", input[i+1]);
		}
	}
	printf("\n");
	return 0;
}