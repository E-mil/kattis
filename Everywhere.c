#include "stdio.h"
#include "string.h"

int compare(char a[], char b[], size_t as, size_t bs);

int main() {
	int n, o;
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &o);
		char a[o][30];
		int x = 0;
		for (int j = 0; j < o; j++) {
			scanf("%s", a[j]);
			for (int k = 0; k < j; k++) {
				if (compare(a[j], a[k], strlen(a[j]), strlen(a[k])) == 1) {
					x--;
					break;
				}
			}
			x++;
		}
		printf("%d\n", x);
	}
}

int compare(char a[], char b[], size_t as, size_t bs) {
	if (as != bs) {
		return 0;
	}
	for (int i = 0; i < as; i++) {
		if (a[i] != b[i]) {
			return 0;
		}
	}
	return 1;
}