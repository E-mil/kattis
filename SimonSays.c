#include "stdio.h"
#include "string.h"

int main() {
	int t;
	scanf("%d\n", &t);
	char output[t][110];
	int oph = 0;
	for (int i = 0; i < t; i++) {
		char input[110];
		gets(input);
		char temp[11];
		for (int j = 0; j < 11; j++) {
			temp[j] = input[j];
		}
		if (strcmp(temp, "Simon says ") == 0) {
			for (int j = 11; j < 110; j++) {
				output[oph][j - 11] = input[j];
			}
			oph++;
		}
	}
	for (int i = 0; i < oph; i++) {
		printf("%s\n", output[i]);
	}
	return 0;
}