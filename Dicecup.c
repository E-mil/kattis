#include "stdio.h"

int main() {
    int a, b;
    scanf("%d %d", &a, &b);
    if (b < a) {
    	int temp = a;
    	a = b;
    	b = temp;
    }
    for (int i = a+1; i < b+2; i++) {
        printf("%d\n", i);
    }
    return 0;
}