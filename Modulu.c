#include "stdio.h"

int main() {
	int x[42];
	for(int i = 0; i < 42; i++) {
		x[i] = 0;
	}
	int temp;
	for(int i = 0; i < 10; i++) {
		scanf("%d", &temp);
		temp %= 42;
		x[temp] = 1;
	}
	int sum = 0;
	for(int i = 0; i < 42; i++) {
		sum += x[i];
	}
	printf("%d\n", sum);
	return 0;
}