#include "stdio.h"
#include "math.h"

int main() {
	long int n;
	scanf("%d", &n);
	int cs[40];
	for (int i = 0; i < 40; i ++) {
		cs[i] = 0;
	}
	for(int i = 0; i < 40; i++) {
		if (n > 0) {
			cs[i] = n % 2;
			n = n / 2;
		} else {
			cs[i] = 2;
			break;
		}
	}
	long int sum = 0;
	for(int i = 39; i >= 0; i--) {
		if (cs[i] == 2) {
			i--;
			for (int k = 0; k <= i; k++) {
				if (cs[i-k] == 1) {
					sum += pow(2, k);
				}
			}
			break;
		}
	}
	printf("%d\n", sum);
	return 0;
}
