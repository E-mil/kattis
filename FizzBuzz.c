#include "stdio.h"
#include "stdlib.h"

int main() {
	char tx[4], ty[4], tn[4];
	scanf("%s %s %s", tx, ty, tn);
	int x = atoi(tx);
	int y = atoi(ty);
	int n = atoi(tn);
	for (int i = 1; i <= n; i++) {
		if (i % x == 0) {
			printf("Fizz");
			if (i % y == 0)
				printf("Buzz");
			printf("\n");
		} else if (i % y == 0) {
			printf("Buzz\n");
		} else {
			printf("%d\n", i);
		}
	}
	return 0;
}