#include "stdio.h"
#include "math.h"
#include "stdlib.h"

int main() {
    char temp[0];
    int n;
    int nbr;
    int sum = 0;
    scanf("%s", temp);
    n = atoi(temp);
    for (int i = 0; i < n; i++) {
        scanf("%s", temp);
        nbr = atoi(temp);
        sum += pow(nbr/10, nbr%10);
    }
    printf("%d\n", sum);
    return 0;
}