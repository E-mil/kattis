#include "stdio.h"

int main() {
	int stones;
	scanf("%d", &stones);
	if(stones % 2 == 1) {
		puts("Alice");
	} else {
		puts("Bob");
	}
	return 0;
}