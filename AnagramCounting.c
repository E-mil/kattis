#include "stdio.h"

long int factorial(int num);
long int nbrAnagrams(char* input);
int nbrUnique(char* input, int size);

int main() {
	char temp[5] = {'a','b','c','d','e'};
	printf("%d\n", nbrUnique(temp, 5));
	return 0;
}

long int nbrAnagrams(char* input) {
	int unique = nbrUnique(input, 5);
}

int nbrUnique(char* input, int size) {
	int sum = 0;
	for (int i = 0; i < size; i++) {
		sum += 1;
	}
	return sum;
}

long int factorial(int num) {
	if (num == 0) {
		return 1;
	}
	return num * factorial(num - 1);
}