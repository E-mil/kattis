#include "stdio.h"
#include "string.h"
#include "stdlib.h"

int main() {
	char input[5][40];
	memset(input, -1, 40*5*sizeof(char));
	char temp[10];
	gets(input[0]);
	gets(input[1]);
	gets(input[2]);
	gets(input[3]);
	gets(input[4]);

	/*Find out nbr of nbrs*/
	int nbrs = 1;
	for(int i = 3; i < 40; i += 3) {
		if (input[0][i] == ' ') {
			nbrs += 1;
		}
		i++;
	}

	/*Read complete number and convert it to int*/
	char number[8];
	for (int i = 0; i < nbrs; i++) {
		int base = i * 3 + i;
		if (input[0][base] == ' ')
			number[i] = '1';
		else if (input[0][base+1] == ' ')
			number[i] = '4';
		else if (input[2][base] == ' ')
			number[i] = '7';
		else if (input[2][base+1] == ' ')
			number[i] = '0';
		else if (input[3][base+2] == ' ')
			number[i] = '2';
		else if (input[1][base] == ' ')
			number[i] = '3';
		else if (input[1][base+2] == ' ' && input[3][base] == ' ')
			number[i] = '5';
		else if (input[1][base+2] == ' ')
			number[i] = '6';
		else if (input[3][base] == ' ')
			number[i] = '9';
		else if (input[3][base+1] == ' ')
			number[i] = '8';
		else {
			printf("BOOM!!\n");
			return 0;
		}
	}
	int iNbr;
	iNbr = atoi(number);

	/*Print output*/
	if (iNbr % 6 == 0)
		printf("BEER!!\n");
	else
		printf("BOOM!!\n");
	return 0;
}