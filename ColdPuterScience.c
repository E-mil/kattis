#include "stdio.h"
#include "string.h"
#include "stdlib.h"

int main() {
	int t;
	scanf("%d\n", &t);
	char numbers[t + t * 8];
	memset(numbers, 0, (t + t*8)*sizeof(char));
	gets(numbers);
	int cold_temps;
	int k = 0;
	for (int i = 0; i < t; i++) {
		char temp[8];
		memset(temp, 0, 8*sizeof(char));
		for (int j = 0; j < 8; j++) {
			if (numbers[k] != ' ') {
				temp[j] = numbers[k];
				k++;
			}
			else {
				k++;
				break;
			}
		}
		if (atoi(temp) < 0)
			cold_temps++;
	}
	printf("%d", cold_temps);
	return 0;
}