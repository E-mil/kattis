#include "stdio.h"

int main() {
	int n, q;
	scanf("%d %d", &n, &q);
	unsigned char primes[n/8+1];
	for (int i = 0; i < n/8+1; i++) {
		primes[i] = 0b11111111;
	}
	int sum = 0;
	for (int i = 1; i < n; i++) {
		if (((primes[i/8] >> (i%8)) & 0b00000001) == 1) {
			sum++;
			int a = i+i+1;
			int b = i+1;
			for (int j = a; j < n; j += b) {
				primes[j/8] = primes[j/8] & (~(0b00000001 << (j%8)));
			}
		}
	}	
	printf("%d\n", sum);
	primes[0] = primes[0] & 0b11111110;
	for (int i = 0; i < q; i++) {
		int temp;
		scanf("%d", &temp);
		printf("%d\n", (primes[(temp-1)/8] >> (temp-1)%8) & 0b00000001);
	}
	return 0;
}