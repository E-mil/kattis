#include "stdio.h"

int main() {
	int n;
	char c;
	scanf("%d %c", &n, &c);
	int sum = 0;
	for (int i = 0; i < n * 4; i++) {
		char a, b;
		char temp[2];
		scanf("%s", &temp);
		a = temp[0];
		b = temp[1];
		if (b == c) {
			if (a == '9') {
				sum += 14;
			} else if (a == 'J') {
				sum += 18;
			}
		}
		if (a == 'A') {
			sum += 11;
		}
		if (a == 'K') {
			sum += 4;
		}
		if (a == 'Q') {
			sum += 3;
		}
		if (a == 'J') {
			sum += 2;
		}
		if (a == 'T') {
			sum += 10;
		}
	}
	printf("%d\n", sum);
	return 0;
}