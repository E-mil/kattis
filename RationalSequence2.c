#include "stdio.h"
#include "math.h"

int main() {
	int t;
	scanf("%d", &t);
	char temp[4];
	int x, y;
	int n[t];
	for (int i = 0; i < t; i++) {
		scanf("%s %d/%d", &temp, &x, &y);
		int j = 0;
		int left[100];
		while (x != 1 || y != 1) {
			if (x < y) {
				y = y - x;
				left[j] = 0;
			} else {
				x = x - y;
				left[j] = 1;
			}
			j++;
		}
		int sum = 0;
		int row = pow(2, j);
		for (int k = 1; k < j + 1; k++) {
			row = row / 2;
			if (left[j - k])
				sum += row;
		}
		n[i] = sum + pow (2, j);
	}
	for (int i = 0; i < t; i++) {
		printf("%d %d\n", i + 1, n[i]);
	}
	return 0;
}