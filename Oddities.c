#include "stdio.h"

int main() {
	int t;
	scanf("%d", &t);
	for (int i = 0; i < t; i++) {
		int temp;
		scanf("%d", &temp);
		if (temp % 2 == 0)
			printf("%d is even\n", temp);
		else
			printf("%d is odd\n", temp);
	}
	return 0;
}