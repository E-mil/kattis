#include "stdio.h"
#include "math.h"

int main() {
	long long int a;
	scanf("%lld", &a);
	printf("%.100g\n", sqrt(a) * 4);
	return 0;
}